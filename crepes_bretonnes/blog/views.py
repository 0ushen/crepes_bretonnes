from django.shortcuts import render
from django.http import HttpResponse
from datetime import datetime
from .forms import ContactForm, ArticleForm
from .models import Article
from django.contrib.auth.models import User, Group
from rest_framework import viewsets
from .serializers import UserSerializer, GroupSerializer


# Create your views here.

def home(request):
    """ Exemple de page non valide au niveau HTML pour que l'exemple soit concis """
    return HttpResponse("""
        <h1>Bienvenue sur mon blog !</h1>
        <p>Les crêpes bretonnes ça tue des mouettes en plein vol !</p>
    """)

def date_actuelle(request):
    return render(request, 'blog/date.html', {'date': datetime.now()})


def contact(request):
    form = ArticleForm(request.POST or None)
    if(form.is_bound):
        if form.is_valid(): 
            form.save()
            envoi = True
    
    return render(request, 'blog/contact.html', locals())

class UserViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = User.objects.all().order_by('-date_joined')
    http_method_names = ['get', 'head']
    serializer_class = UserSerializer


class GroupViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows groups to be viewed or edited.
    """
    queryset = Group.objects.all()
    serializer_class = GroupSerializer